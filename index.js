let http = require("http");

//mock collection of courses

let courses = [

		{
			name: "Python 101",
			description: "Learn Python",
			price: 25000
		},
		{
			name: "ReactJS 101",
			description: "Learn React",
			price: 35000
		},
		{
			name: "ExpressJS 101",
			description: "Learn ExpressJS",
			price: 28000
		}
];

http.createServer(function(request,response){

	console.log(request.url); //allows us to differentiate requests via endpoint
	console.log(request.method); //differentiate requests with http methods

	/*
		HTTP requests are differentiated not only via endpoint but also with their methods

		HTTP Methods simply tells the server what action it must take or what kind of response is needed for the request.
		
		With an HTTP methods we can actually create routes with the same endpoint but with different methods

	*/

		if(request.url === "/" && request.method === "GET"){

			response.writeHead(200,{'Content-Type':'text/plain'});
			response.end("This is the / endpoint. GET method request");

		} else if (request.url === "/" && request.method === "POST"){

			response.writeHead(200,{'Content-Type':'text/plain'});
			response.end("This is the / endpoint. POST method request.");

		}else if (request.url === "/" && request.method === "PUT"){

			response.writeHead(200,{'Content-Type':'text/plain'});
			response.end("This is the / endpoint. PUT method request.")
		}else if (request.url === "/" && request.method === "DELETE"){

			response.writeHead(200,{'Content-Type':'text/plain'});
			response.end("This is the / endpoint. DELETE method request.")
		}else if (request.url === "/courses" && request.method === "GET"){
			//when sending a JSON format data as a response, change the Content-Type of the response to application/json
			response.writeHead(200,{'Content-Type':'application/json'});
			response.end(JSON.stringify(courses));//stringify as JSON our courses array to send it as a response

		}else if (request.url === "/courses" && request.method === "POST"){
			//route to add a new course, we have to receive an input from the client

			//to be able to recieve the request body or the input from the request, we have to add a way to receive that input
			//in NodeJS. this is done in 2 steps

			//requestBody will be a placeholder to contain the data (request body) passed from the client.

			let requestBody="";

			//1st step in receiving data from the request in NodeJS is called the data step
			//data step will read the incoming stream of data from the client and process it so we can save it in the requestBody variable

//part na kinukuha natin kay postman
			request.on('data',function(data){

				console.log(data); //stream of data from the client
				requestBody+=data;//data stream isa saved into the variable as a string -- save data

			})
//part na kinukuha natin kay postman, capture the data
			//end step - this will run once or after the request data has been completely sent from the client:

//kumpleto na si request body at pwede na iprocess
			request.on('end',function(){
				//we have completely received the data from the client and thus requestBody now contain our input
				// console.log(requestBody);

				//initially requestBody is in JSON format. We cannot add this to our courses array becaust it is a string. So, we have to update requestBody with a parsed version of the received JSON format data.
				requestBody = JSON.parse(requestBody);
				// console.log(requestBody);//requestBody is now an OBJECT!!!!!!!!!
				//add the requestBody in the array.
				courses.push(requestBody);

				//check the courses array if we were able to add our requestBody
				// console.log(courses);

				response.writeHead(200,{'Content-Type':'application/json'});
				//send the updated courses array to the client as JSON format
				response.end(JSON.stringify(courses));

			})
			
		}

}).listen(4000);

console.log("Server running at localhost:4000");